package com.nc.aem.consulting.ru.core.servlets;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.adobe.xfa.ut.StringUtils.isEmpty;


@Component( service = Servlet.class,
        immediate = true,
        property = {
                "sling.servlet.paths=/bin/getCovidCasesForState",
                "sling.servlet.methods=" + HttpConstants.METHOD_GET
        })
@ServiceDescription("Simple Get Servlet for Calling third Party API and passing the response back to front End")

public class covidIndiaState extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(covidIndiaState.class);

    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws IOException {

        // Get the request parameters
        String finalResponse = null;
        String url_api = "https://covid19-india-adhikansh.herokuapp.com/states";

        try{
            URL urlForGetRequest = new URL(url_api);

            String readLine = null;
            HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuffer response = new StringBuffer();
                while ((readLine = in .readLine()) != null) {
                    response.append(readLine);
                } in .close();

                finalResponse = response.toString();
                System.out.println("JSON String Result " + finalResponse);
                resp.setContentType("application/json");
                resp.setStatus(200);
                resp.getWriter().write(finalResponse);
            } else {
                System.out.println("Get Command Failed");
            }
        }
        catch (IOException E){
            E.printStackTrace();
        }
    }


}

