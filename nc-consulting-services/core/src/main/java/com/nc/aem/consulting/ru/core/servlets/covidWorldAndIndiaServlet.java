package com.nc.aem.consulting.ru.core.servlets;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.adobe.xfa.ut.StringUtils.isEmpty;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

@Component( service = Servlet.class,
        immediate = true,
        property = {
                "sling.servlet.paths=/bin/getCovidCasesDetails",
                "sling.servlet.methods=" + HttpConstants.METHOD_GET
        })
@ServiceDescription("Simple Get Servlet for Calling third Party API and passing the response back to front End")

public class covidWorldAndIndiaServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(covidWorldAndIndiaServlet.class);

    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws IOException {


        // Get the request parameters
        String country = null;
        String state = null;
        String city = null;
        String finalResponse = null;
        JSONObject tempData = new JSONObject();
        JSONObject indiaObject = new JSONObject();

        country=req.getParameter("country");
        state = req.getParameter("state");
        city = req.getParameter("city");

        String url_api = "https://coronavirus-19-api.herokuapp.com/countries";
        String url_api_india = "https://api.covid19india.org/state_district_wise.json";

        // form URL based on data we are looking for. This is subject to change based on API we would use.
        if(!(isEmpty(country))){
            url_api = url_api + "/" + country;
            if(!(isEmpty(state))){
                url_api = url_api + "/" + state;
                if(!(isEmpty(city))){
                    url_api = url_api + "/" + city;
                }
            }
        }

        URL urlForGetRequest = new URL(url_api);

        String readLine = null;
        HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
        connection.setRequestMethod("GET");
        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer response = new StringBuffer();
            while ((readLine = in .readLine()) != null) {
                response.append(readLine);
            } in .close();

            // format the response
            String formattedResponseWorld = formatResponseAllCountries(response.toString());

            // If country is India then we need to make a call to India API
            if(!(isEmpty(country)) && country.equalsIgnoreCase("INDIA")) {
                readLine = null;
                StringBuffer responseIndia = new StringBuffer();
                URL urlForGetRequestIndia = new URL(url_api_india);
                HttpURLConnection connection1 = (HttpURLConnection) urlForGetRequestIndia.openConnection();
                connection1.setRequestMethod("GET");
                int responseCode1 = connection1.getResponseCode();
                if (responseCode1 == HttpURLConnection.HTTP_OK) {
                    BufferedReader inIndia = new BufferedReader(new InputStreamReader(connection1.getInputStream()));
                    while ((readLine = inIndia.readLine()) != null) {
                        responseIndia.append(readLine);
                    }
                    inIndia.close();
                }
                // pass only India Response as is
                String indiaResponse = responseIndia.toString();

                // Append the india Data to other API Response
               try {
                    LOGGER.error("In try Block ");
                    indiaObject = new JSONObject(response.toString());

                    JSONObject indiaResponseObject= new JSONObject(indiaResponse);
                    indiaObject.put("stateLevel", indiaResponseObject);
                    //indiaObject.put("stateLevel", indiaResponse.replace("\\", ""));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //
                //finalResponse = indiaResponse;
                finalResponse = indiaObject.toString();
            }else{
                finalResponse = formattedResponseWorld;
            }
            // print result
            System.out.println("JSON String Result " + finalResponse);
            resp.setContentType("application/json");
            resp.setStatus(200);
            resp.getWriter().write(finalResponse);
        } else {
            System.out.println("Get Command Failed");
        }
    }

    String formatResponseCountryWise(String response){
        String formattedResponse = "hello";
        //JsonObject convertedObject = new Gson().fromJson(response, JsonObject.class);
        //JsonObject convertedObject = new Gson().fromJson(json, JsonObject.class);
        return formattedResponse;
    }

    String formatResponseAllCountries(String response){

        JsonArray jsonArr = new JsonArray();
        try{
            JsonParser parser = new JsonParser();

            JsonElement rootNode = parser.parse(response);

            //  JsonObject details = rootNode.getAsJsonObject();

            JsonArray countries = rootNode.getAsJsonArray();

            for (JsonElement pa : countries) {

                JsonObject countryObj = pa.getAsJsonObject();

                JsonObject obj = new JsonObject();

                obj.addProperty("country", countryObj.get("country").getAsString());
                obj.addProperty("totalCases", countryObj.get("cases").getAsString());
                obj.addProperty("deaths", countryObj.get("deaths").getAsString());
                if(obj.has("recovered")){
                    obj.addProperty("recovered", countryObj.get("recovered").getAsString());

                }


                obj.addProperty("active", countryObj.get("active").getAsString());
                obj.addProperty("critical", countryObj.get("critical").getAsString());

                jsonArr.add(obj);
            }
        }catch(Exception e){
            LOGGER.error("in error"+e);
            // logger.error( e.printStackTrace());
            // e.printStackTrace();
        }

        return jsonArr.toString();
    }

}
