/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.nc.aem.consulting.ru.core.servlets;

import com.day.cq.commons.jcr.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import static com.adobe.xfa.ut.StringUtils.isEmpty;
import com.google.gson.Gson;
import com.google.gson.JsonArray; 
import com.google.gson.JsonElement; 
import com.google.gson.JsonObject; 
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;


/**
 * Servlet that writes some sample content into the response. It is mounted for
 * all resources of a specific Sling resource type. The
 * {@link SlingSafeMethodsServlet} shall be used for HTTP methods that are
 * idempotent. For write operations use the {@link SlingAllMethodsServlet}.
 */
@Component(service=Servlet.class,
           property={
                   "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                   "sling.servlet.paths=/bin/getCovidDetails",
           })
@ServiceDescription("Simple Demo Servlet")
public class covidServelt extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;
   
	private static final Logger logger = LoggerFactory
			.getLogger(covidServelt.class);
    @Override
    protected void doGet(final SlingHttpServletRequest req,
            final SlingHttpServletResponse resp) throws ServletException, IOException {
        // Get the request parameters
        String country = null;
        String state = null;
        String city = null;
        country=req.getParameter("country");
        state = req.getParameter("state");
        city = req.getParameter("city");		
        String india_covid_api = "https://api.covid19india.org/state_district_wise.json";
        String url_api = "https://coronavirus-19-api.herokuapp.com/countries";
        //String url_api_country = url_api + "/" + country;
		// form URL based on data we are looking for. This is subject to change based on API we would use.
        if(!(isEmpty(country))){
            url_api = india_covid_api;
         
        }
		logger.info("URL API"+url_api);
       URL urlForGetRequest = new URL(url_api);
	   String readLine = null;
	   String formattedResponse=null;
		HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
        connection.setRequestMethod("GET");
        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
			
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer response = new StringBuffer();
            while ((readLine = in .readLine()) != null) {
                response.append(readLine);
            } in .close();
            // print result
            System.out.println("JSON String Result " + response.toString());
			logger.info("Resposne before fromatting"+ response.toString());
			 if(!(isEmpty(country))){
			  formattedResponse = formatResponseCountryWise(response.toString());
			 }
			 else{
				
			  formattedResponse = formatResponseAllCountries(response.toString());
			   logger.info("formattedResponse"+ formattedResponse);
			 }
			  resp.setContentType("application/json");
              resp.setStatus(200);
              resp.getWriter().write(formattedResponse);
        } else {
            System.out.println("Get Command Failed");
        }
	}
	String formatResponseCountryWise(String response){
		String formattedResponse = "hello";
		//JsonObject convertedObject = new Gson().fromJson(response, JsonObject.class);
		//JsonObject convertedObject = new Gson().fromJson(json, JsonObject.class);
	   return formattedResponse;
	}
	
	String formatResponseAllCountries(String response){
		
		JsonArray jsonArr = new JsonArray();	
		try{
	   String formattedResponse = "hello"; 
          
	   JsonParser parser = new JsonParser();
   
       JsonElement rootNode = parser.parse(response);
	   
	 //  JsonObject details = rootNode.getAsJsonObject();

	   JsonArray countries = rootNode.getAsJsonArray();
      
	   for (JsonElement pa : countries) { 
	    
            JsonObject countryObj = pa.getAsJsonObject();
		
            JsonObject obj = new JsonObject();
			
			obj.addProperty("country", countryObj.get("country").getAsString());
			obj.addProperty("totalCases", countryObj.get("cases").getAsString());
			obj.addProperty("deaths", countryObj.get("deaths").getAsString());
			if(obj.has("recovered")){
				obj.addProperty("recovered", countryObj.get("recovered").getAsString());
				
			}
			    
			
			obj.addProperty("active", countryObj.get("active").getAsString());
			obj.addProperty("critical", countryObj.get("critical").getAsString());
			
           jsonArr.add(obj);
         }
	  }catch(Exception e){
		  logger.error("in error"+e);
		 // logger.error( e.printStackTrace());
		 // e.printStackTrace();
	  }
	     
       
         
	   return jsonArr.toString();
	}
}
